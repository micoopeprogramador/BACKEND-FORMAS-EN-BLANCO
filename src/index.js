import app from './app.js'
import { connectDB } from './db.js'
//lista de dominios permitidos
const whiteList = ['https://www.pruebausac.micoopecoban.com', 'https://pruebausac.micoopecoban.com'];

connectDB();
app.listen(3000)
console.log('Server on port',3000)